(ns mongo-to-postgres.core
  (:require [cheshire.core :refer :all]
            [clojure.java.io :as io]))

(def dump-lead-in ["SET statement_timeout = 0;\n"
                   "SET lock_timeout = 0;\n"
                   "SET client_encoding = 'UTF8';\n"
                   "SET standard_conforming_strings = on;\n"
                   "SET check_function_bodies = false;\n"
                   "SET client_min_messages = warning;\n"
                   "SET search_path = public, pg_catalog;\n"
                   "SET default_tablespace = '';\n"
                   "SET default_with_oids = false;\n"
                   "COPY imaging_ui_attachment (agency, collection, company_id, description, dropbox_integration, effective_date, expiration_date, filename, height, identifier, meta_tags, mimetype, picture_counter, anniversary_counter, processed_date, transaction_date, upload_date, username, vector_policy, width, folder_id, agency_code, notice_type, _id, company_level) FROM stdin;\n"])

(defn extract-array-field [row the-kw]
  (let [kw-data   (the-kw row)
        kw-vector (if (vector? kw-data) kw-data (vector kw-data))]
    (if (not-any? nil? kw-vector)
      (str "{\"" (clojure.string/join "," kw-vector) "\"}")
      (str "{}"))))

(defn company-level? [meta-tags]
  (let [tags (if (vector? meta-tags) meta-tags (vector meta-tags))]
    (if (.contains tags "company_level") "True" "False")))

(defn int-ify [value]
  (if (integer? value)
    value
   (Integer/parseInt (re-find #"\d+" value))))

(defn parse-dimensions [row kw]
  (let [kw-data (kw row)]
    (case kw-data
      ""  "\\N"
      0   "\\N"
      nil "\\N"
      (int-ify kw-data))))

(defn set-identifier [{:keys [vector_policy identifier]}]
  (or identifier vector_policy "\\N"))

(defn build-dump-row [row collection]
  (let [dump-row (clojure.string/join "\t" [(extract-array-field row :agency)
                                            collection
                                            (let [company (:company row)]
                                              (if (and company (not= company "null")) (int-ify company) 0))
                                            (:description row "\\N")
                                            (:dropbox_intergration row "False")
                                            (:effective_date row "\\N")
                                            (:expiration_date row "\\N")
                                            (:filename row "")
                                            (parse-dimensions row :height)
                                            (set-identifier row)
                                            (extract-array-field row :meta_tags)
                                            (:mimetype row "application/octet-stream")
                                            (:picture_counter row "\\N")
                                            (:anniversary_counter row "\\N")
                                            (:processed_data row "\\N")
                                            (:transaction_date row "\\N")
                                            (:uploadDate row "\\N")
                                            (:username row "\\N")
                                            (:vector_policy row "\\N")
                                            (parse-dimensions row :width)
                                            (let [folder (:folder row)]
                                              (if folder (int-ify folder) "\\N"))
                                            (:agency_code row "\\N")
                                            (:notice_type row "\\N")
                                            (:_id row)
                                            (company-level? (:meta_tags row))])]
        (str dump-row "\n")))

(defn take-a-dump [collection read-from write-to]
  (with-open [rdr (io/reader (io/resource read-from))
              wtr (io/writer write-to)]
    (doseq [line dump-lead-in]
      (.write wtr line))
    (let [lines (line-seq rdr)]
      (dorun (map #(.write wtr %) (map #(build-dump-row (parse-string % true) collection) lines))))
    (.write wtr "\\.\n")))

(defn -main [& args]
  (if (not= (count args) 3)
    (println "Usage: `lein run <collection> <input-file> <output-file>")
    (take-a-dump (first args) (second args) (last args))))
