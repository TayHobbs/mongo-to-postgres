# mongo-to-postgres

Generate a file with the format of a pg_dump suitable to be loaded into postgres

## Usage

`lein run <collection> <input-file> <output-file>`

- collection: The mongo collection that the data extract came from [policy|management|etc]
- input-file: The data extract from mongo. Should be saved in `resources/`
- output-file: The name of the file that will be output. Saved in the top level directory
